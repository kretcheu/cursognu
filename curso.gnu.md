# Curso GNU/Linux módulo 1 "Essencial"

## Público

O foco do curso é para pessoas que atuam ou pretendem atuar nas posições:

- Sysadmin
- Administradores de rede
- Cloud Native Engineer
- Cloud Engineer
- DevOps Engineer
- Plataform Engineer
- GitOps Engineer
- Hobistas

## Pré-requisitos

- Vontade de aprender.
- Mente aberta para quebrar paradigmas.

## Objetivos

- Começar a construir uma "base sólida" sobre o sistema operacional GNU/Linux.
- Avançar profissionalmente.
- Entender as repercussões e importância de um sistema livre.
- Entender conceitos do sistema e seus componentes.
- Aprimorar a capacidade de resolver problemas.
- Administrar seu próprio sistema.
- Usar o sistema com confiança e controle.
- Ampliar o potencial de uso do sistema.

Dinâmica:
- conceitos
- laboratórios
- atividades individuais
- oficinas
- material de apoio

## Conteúdo
- Introdução / objetivos.
- Conceitos básicos de Software livre.
- Conceitos básicos do sistema GNU.
- Instalação (Debian).
- Princípios do Unix.
- FHS.
- Entendendo Boot.
- Terminal.
- Programas.
- Pacotes.
- Sistemas de arquivos.
- Permissões.
- Processos.
- Módulos do kernel.
- Systemd.
- Solucionando problemas e logs.
- Administração de usuários.
- Acesso remoto.

## Atividades

- São 14 aulas.
- 7 Oficinas, Laboratórios/tira-dúvidas.
- carga horária total ≃ 50h em cada turma.

## Material

- Serão disponibilizados os vídeos das 3 turmas anteriores para os participantes.
- Material de apoio composto de slides, guias e links.

## Apoio e tira-dúvidas

- Participação de grupo do Telegram para interagir e tiras-dúvidas.

-------

## Custo e condições

- R$ 336,00 via pix ou parcelado no cartão de crédito via pagseguro. (já com 20% de desconto)

Obs.: O número de parcelas você pode escolher junto a sua operadora de cartão de crédito.

Pix:
kretcheu@gmail.com

Links de pagamento do PagSeguro:
https://pag.ae/7YnBZkv11


## Contato
Qualquer dúvida entre em contato:

- e-mail: kretcheu@gmail.com
- Telegram: @kretcheu

