# Curso Oficinas GNU/Linux 1
# Setembro/Outubro 2023

## Público

O foco do curso é para pessoas que atuam ou pretendem atuar nas posições:

- Sysadmin
- Administradores de rede
- Cloud Native Engineer
- Cloud Engineer
- DevOps Engineer
- Plataform Engineer
- GitOps Engineer
- Hobistas

## Pré-requisitos

- Vontade de aprender.
- Mente aberta para quebrar paradigmas.
- Conhecimentos equivalentes ao do Curso GNU.
- Disposição para enfrentar desafios.

## Objetivos

- Desenvolver habilidades para solução de problemas.
- Aprimorar os conhecimentos do sistema GNU/Linux.
- Aprimorar conceitos do sistema e seus componentes.
- Ampliar o potencial de uso do sistema.

Dinâmica:
- Oficinas ao vivo.
- material de apoio

## Conteúdo
- Introdução / objetivos.
- Apresentação das oficinas propostas.
- Oficinas propostas

	- boot raw disk.
	  - boot c:
	- Debian openrc
	- thin xdmcp / thin RDP.
	- transplante.
	- Luks.
	- segunda distro com gráfico.
	- recuperar "HD formatado".
	- converter MBR/GPT.
	- recuperar arquivos.
	- erros com apt-get após tentativa de atualizar versão.
	- túneis em ssh pra rodar serviços remotamente.
	- criptografia no envio de e-mails com chaves públicas e privadas.
	- temas escolhidos pelas pessoas inscritas no curso.

## Atividades

- Serão 15 oficinas de 3h aproximadamente.
- carga horária total ≃ 45h

## Material

- As aulas são ao vivo via Jitsi.
- Serão disponibilizados os vídeos para participantes.
- Material de apoio composto de slides, guias e links.

## Datas propostas
Setembro

Data| Dia | Horário| Oficina
:---:|:---:|:---:|:---:
12  | ter | 19h30m    | *** |
14  | qui | 19h30m    | *** |
19  | ter | 19h30m    | *** |
21  | qui | 19h30m    | *** |
26  | ter | 19h30m    | *** |
28  | qui | 19h30m    | *** |

Outubro

Data| Dia | Horário| Oficina
:---:|:---:|:---:|:---:
3   | ter | 19h30m    | *** |
5   | qui | 19h30m    | *** |
17  | ter | 19h30m    | *** |
19  | qui | 19h30m    | *** |
23  | ter | 19h30m    | *** |
26  | qui | 19h30m    | *** |
31  | ter | 19h30m    | *** |

Novembro

Data| Dia | Horário| Oficina
:---:|:---:|:---:|:---:
7    | ter | 19h30m    | *** |
9    | qui | 19h30m    | *** |

-------

## Custo e condições

- R$ 450,00 via pix ou parcelado no cartão de crédito via pagseguro.
**(Meninas tem 30% de desconto)**

Obs.: O número de parcelas você pode escolher junto a sua operadora de cartão de crédito.

Pix:
kretcheu@gmail.com

Links de pagamento do PagSeguro:

- **com 10% de desconto para inscrições até 10 de Setembro.**
https://pag.ae/7ZHJTahTJ

- Sem desconto.
https://pag.ae/7ZHJRuq-o

- com 20% de desconto para alunos de cursos anteriores:
https://pag.ae/7ZHJUaTnJ

## Contato
Qualquer dúvida entre em contato:

- e-mail: kretcheu@gmail.com
- Telegram: @kretcheu

