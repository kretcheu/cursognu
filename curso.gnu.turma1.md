# Curso GNU/Linux módulo 1 "Essencial"
# Julho/Agosto 2022

## Público

O foco do curso é para pessoas que atuam ou pretendem atuar nas posições:

- Sysadmin
- Administradores de rede
- Cloud Native Engineer
- Cloud Engineer
- DevOps Engineer
- Plataform Engineer
- GitOps Engineer
- Hobistas

## Pré-requisitos

- Vontade de aprender.
- Mente aberta para quebrar paradigmas.

## Objetivos

- Começar a construir uma "base sólida" sobre o sistema operacional GNU/Linux.
- Avançar profissionalmente.
- Entender as repercussões e importância de um sistema livre.
- Entender conceitos do sistema e seus componentes.
- Aprimorar a capacidade de resolver problemas.
- Administrar seu próprio sistema.
- Usar o sistema com confiança e controle.
- Ampliar o potencial de uso do sistema.

Dinâmica:
- conceitos
- laboratórios
- atividades individuais
- oficinas
- material de apoio

## Conteúdo
- Introdução / objetivos.
- Conceitos básicos de Software livre.
- Conceitos básicos do sistema GNU.
- Instalação (Debian).
- Princípios do Unix.
- FHS.
- Entendendo Boot.
- Terminal.
- Programas.
- Pacotes.
- Sistemas de arquivos.
- Permissões.
- Processos.
- Módulos do kernel.
- Systemd.
- Solucionando problemas e logs.
- Administração de usuários.
- Acesso remoto.

## Atividades

- Serão 14 aulas de 2h aproximadamente.
- 7 Oficinas, Laboratórios/tira-dúvidas ≃ 4h cada um.
- carga horária total ≃ 56h

## Material

- As aulas são ao vivo via Jitsi.
- Serão disponibilizados os vídeos para participantes.
- Material de apoio composto de slides, guias e links.

## Datas propostas
Julho

Data| Dia | Horário|     Aula |   Laboratório
:---:|:---:|:---:|:---:|:---:
4   | seg | 19h30m    | *** |
6   | qua | 19h30m    | *** |
9   | sáb | 14h       |     | ***
11  | seg | 19h30m    | *** |
13  | qua | 19h30m    | *** |
16  | sáb | 14h       |     | ***
18  | seg | 19h30m    | *** |
20  | qua | 19h30m    | *** |
23  | sáb | 14h       |     | ***
25  | seg | 19h30m    | *** |
27  | qua | 19h30m    | *** |
30  | sáb | 14h       |     | ***

Agosto

Data| Dia | Horário|     Aula |   Laboratório
:---:|:---:|:---:|:---:|:---:
 1  | seg | 19h30m    | *** |
 3  | qua | 19h30m    | *** |
 6  | sáb | 14h       |     | ***
 8  | seg | 19h30m    | *** |
10  | qua | 19h30m    | *** |
13  | sáb | 14h       |     | ***
15  | seg | 19h30m    | *** |
17  | qua | 19h30m    | *** |
20  | sáb | 14h       |     | ***

-------
## calendário
Julho

Seg  | Ter | Qua | Qui | Sex | Sáb
:---:|:---:|:---:|:---:|:---:|:---:|
 [4] |  .  |  [6] | .  |  .  |  [[9]]
[11] |  .  | [13] | .  |  .  | [[16]]
[18] |  .  | [20] | .  |  .  | [[23]]
[25] |  .  | [27] | .  |  .  | [[30]]

Agosto

Seg  | Ter | Qua | Qui | Sex | Sáb
:---:|:---:|:---:|:---:|:---:|:---:|
 [1] |  .  |  [3] | .  |  .  |  [[6]]
 [8] |  .  | [10] | .  |  .  | [[13]]
[15] |  .  | [17] | .  |  .  | [[20]]

-------

```
[aula]
[[Laboratório]]
```

## Custo e condições

- R$ 420,00 via pix ou parcelado no cartão de crédito via pagseguro.
**(Meninas tem 30% de desconto)**

Obs.: O número de parcelas você pode escolher junto a sua operadora de cartão de crédito.

Pix:
kretcheu@gmail.com

Links de pagamento do PagSeguro:

- **com 10% de desconto para inscrições em Junho.**
https://pag.ae/7YnX_jVsL

- Sem desconto.
https://pag.ae/7YngP3cg7

- com 30% de desconto para mulheres:
https://pag.ae/7YngQvy2N

- com 20% de desconto para duplas
https://pag.ae/7YngSJUzs

- com 20% de desconto para alunos de cursos anteriores:
https://pag.ae/7YnBZkv11

## Contato
Qualquer dúvida entre em contato:

- e-mail: kretcheu@gmail.com
- Telegram: @kretcheu

