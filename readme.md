# Curso GNU/Linux módulo 1 "Essencial"
# novembro/dezembro 2022

## Público

O foco do curso é para pessoas que atuam ou pretendem atuar nas posições:

- Sysadmin
- Administradores de rede
- Cloud Native Engineer
- Cloud Engineer
- DevOps Engineer
- Plataform Engineer
- GitOps Engineer
- Hobistas

## Pré-requisitos

- Vontade de aprender.
- Mente aberta para quebrar paradigmas.

## Objetivos

- Começar a construir uma "base sólida" sobre o sistema operacional GNU/Linux.
- Avançar profissionalmente.
- Entender as repercussões e importância de um sistema livre.
- Entender conceitos do sistema e seus componentes.
- Aprimorar a capacidade de resolver problemas.
- Administrar seu próprio sistema.
- Usar o sistema com confiança e controle.
- Ampliar o potencial de uso do sistema.

Dinâmica:
- conceitos
- laboratórios
- atividades individuais
- oficinas
- material de apoio

## Conteúdo
- Introdução / objetivos.
- Conceitos básicos de Software livre.
- Conceitos básicos do sistema GNU.
- Instalação (Debian).
- Princípios do Unix.
- FHS.
- Entendendo Boot.
- Terminal.
- Programas.
- Pacotes.
- Sistemas de arquivos.
- Permissões.
- Processos.
- Módulos do kernel.
- Systemd.
- Solucionando problemas e logs.
- Administração de usuários.
- Acesso remoto.

## Atividades

- Serão 14 aulas de 2h aproximadamente.
- Tira-dúvidas via grupo no Telegram.
- 7 Oficinas, Laboratórios/tira-dúvidas ≃ 3h cada um.
- carga horária total ≃ 49h

## Material

- As aulas são ao vivo via Jitsi.
- Serão disponibilizados os vídeos para participantes.
- Material de apoio composto de slides, guias e links.

## Datas propostas
Novembro

Data| Dia | Horário|     Aula |   Laboratório
:---:|:---:|:---:|:---:|:---:
 7  | seg | 19h30m    | *** |
 9  | qua | 19h30m    | *** |
12  | sáb | 10h       |     | ***
14  | seg | 19h30m    | *** |
16  | qua | 19h30m    | *** |
19  | sáb | 10h       |     | ***
21  | seg | 19h30m    | *** |
23  | qua | 19h30m    | *** |
26  | sáb | 10h       |     | ***
28  | seg | 19h30m    | *** |
30  | qua | 19h30m    | *** |

Dezembro

Data| Dia | Horário|     Aula |   Laboratório
:---:|:---:|:---:|:---:|:---:
 3  | sáb | 10h       |     | ***
 5  | seg | 19h30m    | *** |
 7  | qua | 19h30m    | *** |
10  | sáb | 10h       |     | ***
12  | seg | 19h30m    | *** |
14  | qua | 19h30m    | *** |
17  | sáb | 10h       |     | ***
19  | seg | 19h30m    | *** |
20  | ter | 19h30m    | *** |
21  | qua | 19h30h    |     | ***

-------
## calendário
Novembro

Seg  | Ter | Qua | Qui | Sex | Sáb
:---:|:---:|:---:|:---:|:---:|:---:|
 [7] |  .  |  [9] | .  |  .  | [[12]]
[14] |  .  | [16] | .  |  .  | [[19]]
[21] |  .  | [23] | .  |  .  | [[26]]
[28] |  .  | [30] | .  |  .  |

Dezembro

Seg  | Ter | Qua | Qui | Sex | Sáb
:---:|:---:|:---:|:---:|:---:|:---:|
  .  |  .  |   .  | .  |  .  |  [[3]]
 [5] |  .  |  [7] | .  |  .  | [[10]]
[12] |  .  | [14] | .  |  .  | [[17]]
[19] | [20]|[[21]]| .  |  .  |

-------

```
[aula]
[[Laboratório]]
```

## Custo e condições

- R$ 420,00 via pix ou parcelado no cartão de crédito via pagseguro.
**(Meninas tem 30% de desconto)**

Obs.: O número de parcelas você pode escolher junto a sua operadora de cartão de crédito.

Pix:
kretcheu@gmail.com

Links de pagamento do PagSeguro:

- **com 10% de desconto para inscrições em Outubro.**
https://pag.ae/7YKogS6zq

- Sem desconto.
https://pag.ae/7YngP3cg7

- com 30% de desconto para mulheres:
https://pag.ae/7YngQvy2N

- com 20% de desconto para duplas
https://pag.ae/7YngSJUzs

- com 20% de desconto para ex-alunos de outros cursos:
https://pag.ae/7YnBZkv11

## Contato
Qualquer dúvida entre em contato:

- e-mail: kretcheu@gmail.com
- Telegram: @kretcheu

