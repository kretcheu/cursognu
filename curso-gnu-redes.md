# Curso GNU/Linux + Redes
# Junho/Julho 2023

## Público

O foco do curso é para pessoas que atuam ou pretendem atuar nas posições:

- Sysadmin
- Administradores de rede
- Cloud Native Engineer
- Cloud Engineer
- DevOps Engineer
- Plataform Engineer
- GitOps Engineer
- Hobistas

## Pré-requisitos

- Vontade de aprender.
- Mente aberta para quebrar paradigmas.
- Conhecimentos básicos de rede.

## Objetivos

- Começar a construir uma "base sólida" sobre o sistema operacional GNU/Linux.
- Avançar profissionalmente.
- Entender as repercussões e importância de um sistema livre.
- Entender conceitos do sistema e seus componentes.
- Aprimorar a capacidade de resolver problemas.
- Administrar seu próprio sistema.
- Configurar rede e serviços de rede.
- Usar o sistema com confiança e controle.
- Ampliar o potencial de uso do sistema.

Dinâmica:
- conceitos
- laboratórios
- atividades individuais
- oficinas
- material de apoio

## Conteúdo
- Introdução / objetivos.
- Conceitos básicos de Software livre.
- Conceitos básicos do sistema GNU.
- Instalação (Debian).
- Princípios do Unix.
- FHS.
- Entendendo Boot.
- Terminal.
- Programas.
- Pacotes.
- Sistemas de arquivos.
- Permissões.
- Processos.
- Módulos do kernel.
- Systemd.
- Solucionando problemas e logs.
- Administração de usuários.
- Conceitos de rede TCP/IP.
- Protocolos de rede.
- Configuração de rede.
- Serviços http, dns, ssh, dhcp
- Acesso remoto.

## Atividades

- Serão 14 aulas de 3h aproximadamente.
- Oficinas, Laboratórios/tira-dúvidas.

## Material

- As aulas são ao vivo via Jitsi.
- Serão disponibilizados os vídeos para participantes.
- Material de apoio composto de slides, guias e links.

## Datas propostas
Junho

Data| Dia | Horário|     Aula |   Laboratório
:---:|:---:|:---:|:---:|:---:
13  | ter | 19h30m    | *** |
15  | qui | 19h30m    | *** |
20  | ter | 19h30m    | *** |
22  | qui | 19h30m    | *** |
27  | ter | 19h30m    | *** |
29  | qui | 19h30m    | *** |

Julho

Data| Dia | Horário|     Aula |   Laboratório
:---:|:---:|:---:|:---:|:---:
 4  | ter | 19h30m    | *** |
 6  | qui | 19h30m    | *** |
11  | ter | 19h30m    | *** |
13  | qui | 19h30m    | *** |
18  | ter | 19h30m    | *** |
20  | qui | 19h30m    | *** |
25  | ter | 19h30m    | *** |
27  | qui | 19h30m    | *** |

Agosto

Data| Dia | Horário|     Aula |   Laboratório
:---:|:---:|:---:|:---:|:---:
 1  | ter | 19h30m    | *** |
 3  | qui | 19h30m    | *** |
-------

## calendário
Junho

Seg  | Ter | Qua | Qui | Sex | Sáb
:---:|:---:|:---:|:---:|:---:|:---:|
  .  | [13] |  . | [15] |  .  |  .
  .  | [20] |  . | [22] |  .  |  .
  .  | [27] |  . | [29] |  .  |  .

Julho

Seg  | Ter | Qua | Qui | Sex | Sáb
:---:|:---:|:---:|:---:|:---:|:---:|
  .  |  [4] |  . |  [6] |  .  |  .
  .  | [11] |  . | [13] |  .  |  .
  .  | [18] |  . | [20] |  .  |  .
  .  | [25] |  . | [27] |  .  |  .

Agosto

Seg  | Ter | Qua | Qui | Sex | Sáb
:---:|:---:|:---:|:---:|:---:|:---:|
  .  |  [1] |  . |  [3] |  .  |  .


-------

```
[aula]
[[Laboratório]]
```

## Custo e condições

- R$ 420,00 via pix ou parcelado no cartão de crédito via pagseguro.
**(Meninas tem 30% de desconto)**

Obs.: O número de parcelas você pode escolher junto a sua operadora de cartão de crédito.

Pix:
kretcheu@gmail.com

Links de pagamento do PagSeguro:

- **com 10% de desconto para inscrições até 9 de Junho.**
https://pag.ae/7ZqGDsLnL

- Sem desconto.
https://pag.ae/7YngP3cg7

- com 30% de desconto para mulheres:
https://pag.ae/7YngQvy2N

- com 20% de desconto para duplas
https://pag.ae/7YngSJUzs

- com 20% de desconto para alunos de cursos anteriores:
https://pag.ae/7YnBZkv11

## Contato
Qualquer dúvida entre em contato:

- e-mail: kretcheu@gmail.com
- Telegram: @kretcheu

