Promoção Black Friday dos cursos do professor Kretcheu.
(válida até 30/11)

- curso de redes (módulo 1)
  https://salsa.debian.org/kretcheu/cursoredes1/-/blob/main/redes3.md

- curso GNU/Linux (módulo 1)
  https://salsa.debian.org/kretcheu/cursognu/-/blob/main/curso.gnu.turma3.md

- curso empacotamento Debian (módulo 1)
  https://salsa.debian.org/kretcheu/empacotamento/-/blob/master/curso-turma4.md

- curso empacotamento Debían (módulo 2)
  https://salsa.debian.org/kretcheu/ced2/-/blob/main/ced2.md

Todos com 30% de desconto.

- curso de redes -> R$180,00
. https://pag.ae/7YSvzK1Eq

- curso GNU/Linux -> R$294,00
. https://pag.ae/7YSvBQVYR

- empacotamento Debian 1 -> R$140,00
. https://pag.ae/7YSvDvVm3

- empacotamento Debian 2 -> R$140,00
. https://pag.ae/7YSvDZucN

- Todos por R$ 600,00
. https://pag.ae/7YSvEWAga

Conteúdo:

- os vídeos das aulas das turmas.
- o material de apoio.
- participação no grupo de tira-dúvidas no Telegram.

- 2 aulas ao vivo de tira-dúvidas.
