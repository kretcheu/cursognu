# Curso GNU/Linux módulo 2 "Essencial"

## Público

O foco do curso é para pessoas que atuam ou pretendem atuar nas posições:

- Sysadmin
- Administradores de rede
- Cloud Native Engineer
- Cloud Engineer
- DevOps Engineer
- Plataform Engineer
- GitOps Engineer
- Hobistas

## Pré-requisitos

- Vontade de aprender.
- Mente aberta para quebrar paradigmas.
- Participação no módulo 1 ou conhecimentos equivalentes.

## Objetivos

- Continuar a construir uma "base sólida" sobre o sistema operacional GNU/Linux.
- Avançar profissionalmente.
- Entender as repercussões e importância de um sistema livre.
- Entender conceitos do sistema e seus componentes.
- Aprimorar a capacidade de resolver problemas.
- Administrar seu próprio sistema.
- Usar o sistema com confiança e controle.
- Ampliar o potencial de uso do sistema.

Dinâmica:
- conceitos
- laboratórios
- atividades individuais
- oficinas
- material de apoio

## Conteúdo

- Introdução / objetivos
- Editores de texto puro
- Compilar kernel
- compilando módulos dkms
- usando disco virtual
- criptografia de disco
- LVM
- Raid
- configuração de rede
- testes de conectividade
- Virtualização
- cron
- túnel ssh
- permissões de arquivos

## Atividades

- Serão 12 aulas de 2h aproximadamente.
- 6 Oficinas, Laboratórios/tira-dúvidas ≃ 4h cada um.
- carga horária total ≃ 48h

## Material

- Participação no grupo de apoio no Telegram.
- Serão disponibilizados os vídeos.
- Serão disponibilizados slides, guias e links.

-------

```
[aula]
[[Laboratório]]
```

## Custo e condições

- R$ 420,00 via pix ou parcelado no cartão de crédito via pagseguro.
**(Meninas tem 30% de desconto)**

Obs.: O número de parcelas você pode escolher junto a sua operadora de cartão de crédito.

Pix:
kretcheu@gmail.com

Links de pagamento do PagSeguro:

- Valor cheio.
https://pag.ae/7Y-XFR6b7

- com 30% de desconto para mulheres:
https://pag.ae/7Y-XFfZ15

- com 20% de desconto para duplas
https://pag.ae/7Y-XGQYCP

- com 20% de desconto para alunos de cursos anteriores:
https://pag.ae/7Y-XJ52S7

- Módulo 1 e módulo 2
https://pag.ae/7Y-XKyp2M

## Contato
Qualquer dúvida entre em contato:

- e-mail: kretcheu@gmail.com
- Telegram: @kretcheu

